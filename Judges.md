- Raffi Krikorian (Uber/Twitter) : "The world is broken"

- Lauren Wagner (Meta/Google) : researching the "trust and safety" industry => data ownership

- Ramak Molavi : (Law technologist) : AI governance, sustainable and public good oriented technologies (meaningful AI transparency project)

- Raesetje Sefala : (Distributed AI Research Institute=DAIR) "AI is not inevitable, its harms are preventable" (DAIR)

- Damon Horowitz : (serial entrepreneur/ Philosophy prof) : "humanity should lead technology, not vice versa"

- James Hodsen : (AI for good foundation)

- Deborah Raji : algorithmic bias, AI accountability and algorithmic auditing

- Craig Newmark : (Craiglist) : philanthrophies 50 000$ generous donation
