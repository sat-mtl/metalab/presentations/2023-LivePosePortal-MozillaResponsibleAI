import React from 'react';
import { Container } from 'react-grid-system';
 
class Grid extends React.Component {
    render() {
      const { hasError, idyll, updateProps, ...props }= this.props;
      return (
        <Container>
             {this.props.text || this.props.children}
        </Container>     
    );
  }
}

export default Grid;

/*
<Container>
  <Row>
    <Col sm={4}>
      One of three columns
    </Col>
    <Col sm={4}>
      One of three columns
    </Col>
    <Col sm={4}>
      One of three columns
    </Col>
  </Row>
</Container>
*/