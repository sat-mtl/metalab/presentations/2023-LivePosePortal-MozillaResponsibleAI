import React from 'react';
import D3Component from 'idyll-d3-component';
import * as d3Select from 'd3-selection';
import * as d3Scale from 'd3-scale';

const size = 600;

class CustomD3Component extends D3Component {

  initialize(node, props) {
    this.$body = d3Select.select('body');
    this.$containers = d3Select.selectAll('.idyll-text-container');
    this.$controls = d3Select.selectAll('.sliderContainer');
    this.background = d3Scale.scaleLinear().domain([0, 1]).range(['#444', '#fff']);
    this.color = d3Scale.scaleLinear().domain([1, 0]).range(['#222', '#ccc']);
    this.fontSize = d3Scale.scaleSqrt().domain([0, 1]).range([14, 30]);
    this.maxWidth = d3Scale.scaleSqrt().domain([0, 0.25, 1]).range([600, 800, window.innerWidth - 150]);
    this.$headers = d3Select.selectAll('.article-header');

    setTimeout(() => {
      this.$body.style('opacity', 1);
    })
  }

  update(props, oldProps) {
    const { brightness, fontSize } = props;
    this.$body
      .style('color', this.color(brightness))
      .style('background', this.background(brightness))
      .style('font-size', this.fontSize(fontSize) + 'px')
      .style('line-height', 2 * this.fontSize(fontSize) + 'px');

    // this.$body
    //   .selectAll('a')
    //   .style('color', this.color(brightness))
    this.$headers
    .style('background', this.background(brightness))
    .style('color', this.color(brightness))
    d3Select.selectAll('.sliderContainer').selectAll('.button')
    .style('background', this.background(brightness))
    .style('color', this.color(brightness))

    // this.$controls

    //   .style('color', this.color(brightness));

    this.$controls.selectAll('button')
      // .style('border-bottom-color', this.color(brightness))
      .style('color', this.color(brightness));

    // this.$controls.selectAll('.button').style('border-bottom-color', this.color(brightness))

    this.$containers.style('max-width', this.maxWidth(fontSize) + 'px');

    // this.$containers.selectAll('li')
    //   .style('filter', `invert(${1 - brightness})`);
  }
}

export default CustomD3Component;
