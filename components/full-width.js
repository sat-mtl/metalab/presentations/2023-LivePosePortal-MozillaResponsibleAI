import React from 'react';

class FullWidth extends React.Component {
  render() {
    const { idyll, hasError, updateProps, className, children, style, ...props } = this.props;
    return (
        <div className={'fullWidth'} >
            { children }
            <div className={'fullWidthEnd'} >&nbsp;</div>
        </div>
    );
  }
}

export default FullWidth;
