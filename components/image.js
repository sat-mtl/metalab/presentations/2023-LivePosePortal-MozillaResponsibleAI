import React from 'react';

class Image extends React.Component {
  render() {
    let caption;
    if (this.props.caption !== undefined) {
      caption = <figcaption className={`imageCaption {this.props.className}`}>{this.props.caption}</figcaption>;
    }
    return (
      <figure className="imageDiv">
        <img 
          src={this.props.src}
          alt={this.props.alt}
        />
        {caption}
      </figure>
    );
  }
}

export default Image;
