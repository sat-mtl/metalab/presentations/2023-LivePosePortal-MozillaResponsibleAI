# LivePose Portal

Explorable explanation for the [Mozilla Responsible AI Challenge](https://future.mozilla.org/builders-challenge/).

## Explorable Explanation

Source: [index.idyll](index.idyll)

### Requirements

- Install Node.js and NPM and sed.
```
sudo apt install 
```
- Install dependencies (once or after upgrades):
```
npm i
```
- (Temporary) Fix generation of `build/static/idyll_index.js` otherwise missing with `npm run build`:
```
sed -i 's/process.exit/\/\/process.exit/' node_modules/idyll/bin/cmds/build.js
```

### Development

Write markdown and React in [index.idyll](index.idyll) while [idyll](https://idyll-lang.org)'s local server converts to html.

- Run the local server:
```
npm run start
```

### Release

- Convert idyll to html:
```
npm run build
```
