# LivePosePortal Pitch: Storyboard

LivePose Portal trains participants and deep-learning models for camera-based group interactivity in the immersive arts.

## First draft along Judging Criteria

### Responsible AI Score: (0-LOW 10-HIGH)

> Is the project developed responsibly and incorporates the guidelines of Responsible AI? 
> Bonus: does the project substantively address any of the risk areas of AI and help solve them?

#### Slide

##### Text 

(to shorten)

###### 1. Agency

With LivePose Portal, people have control over how they use the AI, as they contribute their own version of poses and actions in a portal that welcomes them, before entering associated immersive art installations whose AI is re-trained on the spot to become compatible with their movements. People have control over how their data is used as we are planning to design an explorable explanation that would explain people during their visit in the LivePose Portal how the re-training of the model with their individual poses and actions works, so that they have control over the algorithm’s output.

###### 2. Accountability

One first step towards transparency into how our AI system works is that most components of LivePose Portal are open source and will remain so, except for NVIDIA components. A second step is conveyed through the explorable explanation that provides people with agency. We are set up to support accountability when things go wrong: we want to put in place a reporting system, so that anybody can re-visit the Portal to flag the specific time and location inside the associated artwork and a description of what went wrong, so that hosts of the LivePose Portal are informed on how to correct the underlying issues.

###### 3. Privacy

We are collecting people’s data by recording videos of their poses and actions, associated to a unique identifier, so that videos are anonymous, and they can retract their recordings by contacting the hosts who deployed the LivePose Portal instance, and the model can be “de-trained” from poses and actions once individuals leave the premises. We are storing their data directly on the on-board memory of the instance of LivePose Portal, decentralized from remote servers. We are not sharing people’s data.

###### 4. Fairness

We hope to minimize the existing bias reflected by our system, to overcome the underlying discrimination embedded in its third-party components, and to downsize the impact on marginalized communities, by the design of LivePose Portal itself, where people in all their diversity, contribute to make the associated immersive art installations work for them. We think that computing and human labor used to build our AI system has low vulnerability to exploitation and overwork: people visiting LivePose Portal contribute their interpretation of poses and actions through a gentle physical work-out, which empowers them to explore the associated immersive art installation once they exit the portal. We try to minimize our impact by using lower-power edge devices, and using lower frame-rates for action detection while still preserving a magical interactive experience within the associated immersive art installations.

###### 5. Safety

Our main safety net to prevent bad actors from carrying out attacks by exploiting our system is in the localized instance of portals, deployed next to their associated immersive art installations, where human agents are often hired to ensure the safety of participants (and of the artifacts).

##### Media

This teaser from 2002 by Maria Takeuchi showcases her project that was ongoing at the time and supported by through the artistic residency program at the Society for Arts and Technology (SAT). From our perspective, her artwork may be one interpretation on how people “visualize” the connectivities between people and artificial intelligence. 

https://www.youtube.com/watch?v=3RB-YGjyRL8

### Technical Merit: (0-LOW 5-HIGH) 

> Does the project have a sound technical backbone and architecture and can scale to the needed customer base

In order to implement the project we propose a technical implementation based on dedicated hardware and specific architecture :

- Orthogonal gradient descent for continual learning allows us to add the new data training without retraining the model from scratch
- An unlearning mechanism allowing the deletion of poses and actions once individuals leave the premises by strategically limiting the influence of a data point in the training procedure.
- a physical portal prototype with UX designed to make the experience playful and educative;
- NVIDIA Jetson Xavier NX edge devices as cores of the physical portal for portable inferences and transportable deployment;
- LivePose: our open-source tool for democratizing pose detection for multimedia arts and telepresence applications 
with MMAction2: a tool that we have recently learned to own, to train action detection with databases of video movements recorded with LivePose, with our recognition configuration currently relying on the Two-Stream Inflated 3D ConvNet (I3D)


### User Experience: (0-LOW 5-HIGH)

> Does the project center the user it targets and deliver a positive user experience

### Team Score (0-5) (0-LOW 5-HIGH)

> Is the individual or members of the project team seem suitable for the proposed project and/or plans are in place to pull in outside expertise where needed

#### Slide

##### Text

Our team members’ pathways joined at the Society for Arts and Technology, a unique venue in North America that hosts immersive arts in hybrid settings: locally in a dome and remotely with telepresence. 

##### Image

Exploded view of a virtual representation of the physical spaces at the Society for Arts and Technology (SAT).

![](static/images/sat.svg)

#### Slide

##### Text

While our team brings interdisplinary skills in graphic design, research and development in human-computer interaction and AI; our main challenge in terms of team composition is that we are all part of a visible majority.

##### Image

Team photo in Satellite Hub, SAT's fork of Mozilla Hubs. 

![](static/images/group_photo.png)

#### Slide

##### Text

We plan to run participatory design workshops so that people from visible minorities, with specific accessibility needs, can own LivePose Portal. 

##### Image

Photos by Sébastien Roy of two projects, Le chant du canevas and Percepto, during HACKLAB21, a hackaton on digital arts and inclusion, co-organized by two non-profit organizations Music Motion and the SAT.

![](static/images/le_chant_du_canevas_mediapipe.png)

![](static/images/le_chant_du_canevas_mmpose.png)

![](static/images/percepto_mediapipe.png)

![](static/images/percepto_mmpose.png)

### Impact Score (0-LOW 5-HIGH)

> Does the project include a clear understanding of outcomes and the social impact it will have.

